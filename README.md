# Solar Sensor Monitor

Simple code that expose on a web server sensor data on a microcontroller powered by a Li-Po battery and a solar panel:

- temperature (°C)
- relative humidity (%H)
- light (lux)
- battery (V)

[`datalog.py`](datalog.py) stores in `data.csv` the data every 60 seconds.

## Data vizualization

Data can be viewed [here](https://solar-sensor-monitor-loris-croce-e657da1b22488c4fe2025683048d2c.pages.mia.inra.fr/)

## Hardware

- [Feather ESP32-S3](https://www.adafruit.com/product/5323)
- [AHT20](https://www.adafruit.com/product/4566)
- [VEML 7700](https://www.adafruit.com/product/4162)
- [bq24074](https://www.adafruit.com/product/4755)
- [Voltaic P126](https://www.adafruit.com/product/5366)
- [cable](https://www.adafruit.com/product/743)
- [qwiic cables (x2)](https://www.adafruit.com/product/4399)
- [battery](https://www.makeblock.com/products/li-polymer-battery-for-mbot)

## Images

![Solar panel and light sensor](img1.jpg)
![ESP32, battery, charger and temperature sensor](img2.jpg)