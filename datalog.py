import requests
import time
import csv
import os

header = ["timestamp", "temperature", "humidity", "battery", "light"]
data = {}

os.system("nmcli dev wifi connect Feather\ ESP32-S3 password P4ssW0rd!")
time.sleep(2)

with open("data.csv", "w", encoding="UTF-8", newline="") as f:
    writer = csv.writer(f)
    writer.writerow(header)

while True:
    data["timestamp"] = time.time()
    for i in header:
        if i != "timestamp":
            data[i] = requests.get("http://192.168.4.1/" + i).text
    with open("data.csv", "a", encoding="UTF-8", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(data.values())
    time.sleep(60)
