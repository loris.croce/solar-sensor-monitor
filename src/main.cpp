#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Wire.h>
#include <Adafruit_MAX1704X.h>
#include <Adafruit_AHTX0.h>
#include <Adafruit_VEML7700.h>

Adafruit_AHTX0 aht;
Adafruit_VEML7700 veml = Adafruit_VEML7700();
Adafruit_MAX17048 maxlipo;

const char *ssid = "Feather ESP32-S3";
const char *password = "P4ssW0rd!";

AsyncWebServer server(80);

String getHumidity()
{
    sensors_event_t hum, temp;
    aht.getEvent(&hum, &temp);
    return String(hum.relative_humidity);
}

String getTemperature()
{
    sensors_event_t hum, temp;
    aht.getEvent(&hum, &temp);
    return String(temp.temperature);
}

void setup()
{
    Serial.begin(115200);

    WiFi.softAP(ssid, password);

    Serial.println(F("\nAdafruit Battery Monitor simple demo"));

    if (!maxlipo.begin())
        Serial.println(F("Couldnt find Adafruit MAX17048"));

    if (!aht.begin())
        Serial.println("Could not find AHT? Check wiring");

    if (!veml.begin())
        Serial.println("VEML sensor not found");

    IPAddress IP = WiFi.softAPIP();
    Serial.print("Access Point IP address: ");
    Serial.println(IP);

    server.on("/battery", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  String response = String(maxlipo.cellVoltage());
                  request->send(200, "text/plain", response); });

    server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  String response = getTemperature();
                  request->send(200, "text/plain", response); });

    server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  String response = getHumidity();
                  request->send(200, "text/plain", response); });

    server.on("/light", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  String response = String(veml.readLux());
                  request->send(200, "text/plain", response); });

    server.begin();
}

void loop()
{
}
